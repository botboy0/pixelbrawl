package com.hgs.Model.Networking;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.hgs.Model.PixelBrawl;
import com.hgs.Model.Scenes.SceneType;

import java.util.HashMap;

interface Command {
    void runCommand(String[] args);
}

public class CommandHandler {
    PixelBrawl pb;
    String[]args;


    HashMap<String, Command> commandlist = new HashMap<String, Command>();

    public CommandHandler(PixelBrawl pb) {
        this.pb=pb;
        commandlist.put("regmsg", new Command() {
            @Override
            public void runCommand(String[] args){
                regmsg(args);}
        });

        commandlist.put("logmsg", new Command() {
            @Override
            public void runCommand(String[] args) {
                logmsg(args); }
        });

        commandlist.put("updatematches", new Command() {
            @Override
            public void runCommand(String[] args) {
                updatematches(args); }
        });

        commandlist.put("updateplayer", new Command() {
            @Override
            public void runCommand(String[] args) {
                updateplayer(args); }
        });

        commandlist.put("logout", new Command() {
            @Override
            public void runCommand(String[] args) {
                logout(args); }
        });

        commandlist.put("match", new Command() {
            @Override
            public void runCommand(String[] args) {
                match(args); }
        });

        commandlist.put("accountmsg", new Command() {
            @Override
            public void runCommand(String[] args) {
                accountmsg(args); }
        });

        commandlist.put("upgrademsg", new Command() {
            @Override
            public void runCommand(String[] args) {
                upgrademsg(args); }
        });
    }

    public void execute(String cmd) {
        String[] splitcommand = cmd.split(" ");
        String command = splitcommand[0];
        args = new String[splitcommand.length-1];
        for (int i = 0; i < args.length; i++)args[i]=splitcommand[i+1];
        try {
            Gdx.app.log("",cmd);
            commandlist.get(command).runCommand(args);
        } catch (Exception e) {
           Gdx.app.log("",command+" failed");
            for (String arg:args
                 ) {
                Gdx.app.log("",arg);
            }

        }
    }

    //Eingehende Kommandos

    private void regmsg(String[] args) {
       pb.registerscene.getStatuslabel().getLabel().setColor(Color.RED);

       if (args[1]==String.valueOf(1))
       {
           pb.registerscene.getStatuslabel().getLabel().setText("User "+args[0]+" already exists");
       }


       if (args[1]==String.valueOf(0))
       {
           pb.registerscene.getStatuslabel().getLabel().setColor(Color.GREEN);
           pb.registerscene.getStatuslabel().getLabel().setText("User "+args[0]+" registered successfully");

           pb.client.login();
       }

    }
    private void logmsg(String[] args){
        pb.loginscene.getStatuslabel().getLabel().setColor(Color.RED);

        if (args[1]==String.valueOf(2))
        {
            pb.loginscene.getStatuslabel().getLabel().setText("User "+args[0]+" doesnt exist");
        }

        if (args[1]==String.valueOf(1))
        {
            pb.loginscene.getStatuslabel().getLabel().setText("Incorrect password for "+args[0]);
        }

        if (args[1]==String.valueOf(0))
        {
            pb.loginscene.getStatuslabel().getLabel().setColor(Color.GREEN);
            pb.loginscene.getStatuslabel().getLabel().setText("User "+args[0]+" logged in successfully");
            pb.client.login();
            pb.titlescene.reset();
        }
    }

    private void accountmsg(String[] args) {
        pb.confirmscene.getStatuslabel().getLabel().setColor(Color.RED);

        if (args[1]==String.valueOf(3))
        {
            pb.confirmscene.reset();
            pb.confirmscene.getStatuslabel().getLabel().setColor(Color.GREEN);
            pb.confirmscene.getStatuslabel().getLabel().setText("Password Successfully Changed");

        }

        if (args[1]==String.valueOf(2))
        {
            pb.confirmscene.getStatuslabel().getLabel().setText("Name Already Exists");

        }

        if (args[1]==String.valueOf(1))
        {
            pb.confirmscene.getStatuslabel().getLabel().setText("Incorrect password");
        }

        if (args[1]==String.valueOf(0))
        {
            pb.confirmscene.reset();
            pb.confirmscene.getStatuslabel().getLabel().setColor(Color.GREEN);
            pb.confirmscene.getStatuslabel().getLabel().setText("Name Successfully Changed");
        }
    }

    private void updatematches(String[] args) {
        pb.matchmakingscene.getMatches().setitems(args);
    }

    private void logout(String[] args) {
        pb.client.logout();
        pb.loginscene.reset();
        pb.registerscene.reset();
    }

    private void updateplayer(String[] args) {
        pb.client.update(args);
    }

    private void match(String[] args) {
        switch (args[0]) {
            case "wait":
                pb.matchmakingscene.hidematchlist();
                pb.lobbyscene.showlobby();
                pb.matchmakingscene.setOpen(true);
                break;
            case "close":
                pb.matchmakingscene.showmatchlist();
                pb.lobbyscene.hidelobby();
                pb.matchmakingscene.setOpen(false);
                break;
            case "begin":
                pb.matchmakingscene.showmatchlist();
                pb.lobbyscene.hidelobby();
                pb.screen.clearscenes();
                pb.screen.showscene(SceneType.BATTLE);
                pb.battlescene.beginmatch(args[1],args[2]);
                pb.matchmakingscene.setOpen(false);
                break;
            case "end":
                pb.screen.clearscenes();
                pb.battlescene.reset();
                pb.client.setIngame(false);
                pb.screen.showscene(SceneType.MENU);
                pb.screen.showscene(SceneType.STATUS);
                pb.screen.showscene(SceneType.MATCHMAKING);
                pb.matchmakingscene.showmatchlist();
                pb.matchmakingscene.getMatches().requestfocus(pb);
                break;
            case "attack":
                pb.battlescene.animateattack(args[1],args[2],Integer.parseInt(args[3]));
                break;
            case "timerupdate":
                pb.battlescene.setTurntimeleft(Integer.parseInt(args[1]));
                break;
            case "healthupdate":
                break;

            default:
                break;
        }
    }

    private void upgrademsg(String[] args) {
        int stat = Integer.parseInt(args[0]);
        int status=Integer.parseInt(args[1]);
        switch (status){
            case 0:
                pb.shopscene.getShops().get(stat).getSuccesslabel().getLabel().setColor(0f,1f,0.5f,1f);
                pb.shopscene.getShops().get(stat).getSuccesslabel().getLabel().setText("Success");
                break;
            case 1:
                pb.shopscene.getShops().get(stat).getSuccesslabel().getLabel().setColor(Color.RED);
                pb.shopscene.getShops().get(stat).getSuccesslabel().getLabel().setText("No Money");
                break;
            case 2:
                pb.shopscene.getShops().get(stat).getSuccesslabel().getLabel().setColor(Color.RED);
                pb.shopscene.getShops().get(stat).getSuccesslabel().getLabel().setText("Fail");
            default:
                break;
        }
    }
}
