package com.hgs.Model.Networking;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Array;
import com.hgs.Model.PixelBrawl;
import com.hgs.Model.Scenes.SceneType;
import com.sksamuel.gwt.websockets.Websocket;
import com.sksamuel.gwt.websockets.WebsocketListener;


public class Client {
    PixelBrawl pb;
    boolean online = false;
    boolean ingame;
    String Username;
    String Password;
    float Money;
    int Wins;
    int Losses;
    Array<Integer>levels=new Array<Integer>();
    Websocket socket;
    CommandHandler handler;

    public Client(PixelBrawl pb, String url) {
        this.pb = pb;
        socket = new Websocket(url);
        handler = new CommandHandler(pb);
        socket.open();
        socket.addListener(new WebsocketListener() {


            @Override
            public void onClose() {
                logout();
            }

            @Override
            public void onMessage(String msg) {
                handler.execute(msg);
            }

            @Override
            public void onOpen() {
                Gdx.app.log("Status", "Connected");
            }
        });
    }

    public Websocket getSocket() {
        return socket;
    }

    public void sendCommand(String cmd, String[] args) {
        StringBuilder message = new StringBuilder();
        message.append(cmd);
        for (int i = 0; i < args.length; i++) {
            message.append(" " + args[i]);
        }
        socket.send(message.toString());
    }

    public void login() {
        pb.statusscene.setStatus(Username, Money, Wins, Losses);
        pb.screen.clearscenes();
        pb.screen.showscene(SceneType.MENU);
        pb.screen.showscene(SceneType.STATUS);
        pb.screen.showscene(SceneType.MATCHMAKING);
        pb.matchmakingscene.showmatchlist();
        pb.matchmakingscene.getMatches().requestfocus(pb);
    }

    public void logout() {
        pb.screen.clearscenes();
        pb.loginscene.reset();
        pb.registerscene.reset();
        pb.screen.showscene(SceneType.TITLE);

    }

    public void update(String[] data) {
        Username = data[0];
        Password = data[1];
        Money = Float.parseFloat(data[2]);
        Wins = Integer.parseInt(data[3]);
        Losses = Integer.parseInt(data[4]);
        levels.clear();
        for (int i = 5;i<=10;i++){
            levels.add(Integer.parseInt(data[i]));
        }
        pb.shopscene.setlevels(levels);
        pb.statusscene.setStatus(Username,Money,Wins, Losses);
        pb.shopscene.setMoney(Money);
    }

    public boolean isIngame() {
        return ingame;
    }

    public void setIngame(boolean ingame) {
        this.ingame = ingame;
    }
}
