package com.hgs.Model.UI;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.hgs.Model.PixelBrawl;

public class UIPlayerbar extends UIComponent {
    Container<Table> container;
    Image background;
    Table t;
    UILabel playerlabel;
    UILabel vslabel;
    UILabel opponentlabel;

    Array<UIComponent> components;
    public UIPlayerbar(Table table, String name, Vector2 size) {
        super(table, name, size);
        setStyle();
    }

    public UIPlayerbar(Table table, String name, Vector2 size, int[] padding) {
        super(table, name, size, padding);
        setStyle();
    }

    @Override
    void setStyle() {
        t=new Table();
        t.top();
        t.setDebug(false);
        background= new Image(PixelBrawl.skin.getDrawable("textfield"));
        components= new Array<UIComponent>();
        playerlabel = new UILabel("", Align.center,t,"",new Vector2(size.x/5*2,size.y),0.5f,new int[]{0,0,0,0});
        vslabel = new UILabel("VS", Align.center,t,"vslabel",new Vector2(size.x/5,size.y),1f,new int[]{0,0,0,0});
        opponentlabel = new UILabel("botboy0", Align.center,t,"opponentlabel",new Vector2(size.x/5*2,size.y),0.5f,new int[]{0,0,0,0});
        t.add(playerlabel.getlabelcontainer());
        t.add(vslabel.getlabelcontainer()).expandX();
        t.add(opponentlabel.getlabelcontainer());
        container= new Container<Table>(t);
        container.setBackground(background.getDrawable());
        container.top();
        actor=container;
        components.add(playerlabel,vslabel,opponentlabel);
    }

    @Override
    public void scale(float ratio) {
        table.getCell(actor).size((int) Math.ceil(size.x*ratio),(int)Math.ceil(size.y*ratio));
        table.getCell(actor).padTop(padding[0]*ratio);
        table.getCell(actor).padRight(padding[1]*ratio);
        table.getCell(actor).padBottom(padding[2]*ratio);
        table.getCell(actor).padLeft(padding[3]*ratio);
        for (UIComponent component:components)component.scale(ratio);
    }

    public UILabel getPlayerlabel() {
        return playerlabel;
    }

    public UILabel getOpponentlabel() {
        return opponentlabel;
    }

    public Container<Table> getContainer() {
        return container;
    }
}
