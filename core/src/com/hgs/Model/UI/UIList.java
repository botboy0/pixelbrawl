package com.hgs.Model.UI;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.List;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.hgs.Model.PixelBrawl;

public class UIList extends UIComponent{
    float scale;
    Array<String>items;
    List.ListStyle style;
    List<String> list;
    Container<ScrollPane>container;
    ScrollPane pane;

    public UIList(Table table, String name, Vector2 size,float scale) {
        super(table, name, size);
        this.scale=scale;
        setStyle();
    }

    public UIList(Table table, String name, Vector2 size, int[] padding,float scale) {
        super(table, name, size, padding);
        this.scale=scale;
        setStyle();
    }

    @Override
    void setStyle() {
        items= new Array<String>();
        style= new List.ListStyle();
        style.font = PixelBrawl.font;
        style.selection= PixelBrawl.skin.getDrawable("selected");
        list = new List<String>(style);
        list.setAlignment(Align.center);
        pane=new ScrollPane(list);
        container=new Container<ScrollPane>();
        container.setActor(pane);
        container.fill();
        container.setBackground(PixelBrawl.skin.getDrawable("roomlist"));
        actor=container;
    }

    @Override
    public void scale(float ratio) {
        table.getCell(actor).size((int) Math.ceil(size.x*ratio),(int)Math.ceil(size.y*ratio));
        table.getCell(actor).padTop(padding[0]*ratio);
        table.getCell(actor).padRight(padding[1]*ratio);
        table.getCell(actor).padBottom(padding[2]*ratio);
        table.getCell(actor).padLeft(padding[3]*ratio);
        style.font.getData().setScale(ratio*scale);

    }

    public void setitems(String[]itemlist){
       clearitems();
        for (int i=0;i<itemlist.length;i++)items.add(itemlist[i]);
        list.setItems(items);
    }
    public void clearitems(){
        items.clear();
        list.setItems(items);
    }

    public List<String> getList() {
        return list;
    }

    public Container<ScrollPane> getContainer() {
        return container;
    }

    public ScrollPane getPane() {
        return pane;
    }
    public void requestfocus(PixelBrawl pb){
        pb.screen.getStage().setScrollFocus(pane);
    }

}
