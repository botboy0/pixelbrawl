package com.hgs.Model.UI;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.MoveByAction;
import com.badlogic.gdx.scenes.scene2d.actions.RotateByAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.hgs.Model.PixelBrawl;

public class Animation {
    Stage stage;
    Image image;
    MoveByAction moveact;
    RotateByAction rotateact;
    float rotation;
    Vector2 size;
    Vector2 startpos;
    Vector2 endpos;
    float distancex;
    float distancey;
    float progress=0;
    boolean started;
    public Animation(Stage stage, String imgname, Vector2 size, Vector2 startpos, Vector2 endpos, float rotation){
        this.stage=stage;
        this.image= new Image(PixelBrawl.skin.getDrawable(imgname));
        this.size=size;
        image.setSize(size.x,size.y);
        image.setOrigin(image.getWidth()/2,image.getHeight()/2);
        this.startpos=startpos.sub(image.getWidth()/2,image.getHeight()/2);
        this.endpos=endpos.sub(image.getWidth()/2,image.getHeight()/2);
        image.setPosition(this.startpos.x,this.startpos.y);
        distancex=this.endpos.x-this.startpos.x;
        distancey=this.endpos.y-this.startpos.y;
        this.rotation=rotation;
    }
    public void begin(){
        stage.addActor(image);
        started=true;
    }
    public void step(float deltatime){
        if (started){
            if (progress<1){
                image.setPosition(startpos.x+(distancex*progress),startpos.y+(distancey*progress));
                image.setRotation(rotation*progress);
                progress+=deltatime;
            }
            else {

                image.remove();
                started=false;
            }
        }
    }
    public void update(Vector2 startpos, Vector2 endpos, float ratio){
        image.setSize(size.x*ratio,size.y*ratio);
        image.setOrigin(image.getWidth()/2,image.getHeight()/2);
        this.startpos=startpos.sub(image.getWidth()/2,image.getHeight()/2);
        this.endpos=endpos.sub(size.x*ratio/2,size.y*ratio/2);
        distancex=this.endpos.x-this.startpos.x;
        distancey=this.endpos.y-this.startpos.y;

    }


}
