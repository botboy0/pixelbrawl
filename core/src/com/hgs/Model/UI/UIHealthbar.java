package com.hgs.Model.UI;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.hgs.Model.PixelBrawl;

public class UIHealthbar extends UIComponent {
    ProgressBar pb;
    public UIHealthbar(Table table, String name, Vector2 size) {
        super(table, name, size);
        setStyle();
    }

    public UIHealthbar(Table table, String name, Vector2 size, int[] padding) {
        super(table, name, size, padding);
        setStyle();
    }

    @Override
    void setStyle() {
        ProgressBar.ProgressBarStyle prgstyle = new ProgressBar.ProgressBarStyle();
        prgstyle.background = PixelBrawl.skin.getDrawable("healthbg");
        prgstyle.knobBefore = PixelBrawl.skin.getDrawable("health");
        pb = new ProgressBar(0,100,0.01f,true,prgstyle);
        pb.setValue(1);
        actor= pb;
        actor.setSize(size.x,size.y);
    }

    @Override
    public void scale(float ratio) {
        table.getCell(actor).size((int) Math.ceil(size.x*ratio),(int)Math.ceil(size.y*ratio));
        table.getCell(actor).padTop(padding[0]*ratio);
        table.getCell(actor).padRight(padding[1]*ratio);
        table.getCell(actor).padBottom(padding[2]*ratio);
        table.getCell(actor).padLeft(padding[3]*ratio);


        ProgressBar.ProgressBarStyle pbstyle = pb.getStyle();
        pbstyle.background.setMinWidth(table.getCell(actor).getPrefWidth());
        pbstyle.background.setMinHeight(table.getCell(actor).getPrefHeight());
        pbstyle.knobBefore.setMinWidth(table.getCell(actor).getPrefWidth());
        pbstyle.knobBefore.setMinHeight(table.getCell(actor).getPrefHeight()/100);
        pb.setStyle(pbstyle);
        actor= pb;

    }

    public ProgressBar getPb() {
        return pb;
    }
}
