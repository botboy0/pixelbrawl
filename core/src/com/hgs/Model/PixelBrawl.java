package com.hgs.Model;

import com.badlogic.gdx.*;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.Array;
import com.hgs.Model.Networking.Client;
import com.hgs.Model.Scenes.*;


public class PixelBrawl extends Game {
	public static int oWIDTH;
	public static int oHEIGHT;
	public static int WIDTH;
	public static int HEIGHT;
	public static  BitmapFont font;
	public static  BitmapFont font1;
	public static TextureAtlas gameAtlas;
	public static TextureAtlas UIAtlas;
	public static  Skin skin;
	public static SpriteBatch batch;
	public GameScreen screen;
	public Client client;
	public Array<Scene>scenes;
	public TitleScene titlescene;
	public RegisterScene registerscene;
	public LoginScene loginscene;
	public MenuScene menuscene;
	public StatusScene statusscene;
	public MatchmakingScene matchmakingscene;
	public LobbyScene lobbyscene;
	public ShopScene shopscene;
	public AccountScene accountscene;
	public ConfirmScene confirmscene;
	public BattleScene battlescene;
	@Override
	public void create () {
		oWIDTH = 1920;
		oHEIGHT = 1080;
		WIDTH = Gdx.graphics.getWidth();
		HEIGHT = Gdx.graphics.getHeight();

		client= new Client(this,"ws://134.255.218.57:5000");
		scenes= new Array<Scene>();
		Gdx.app.setLogLevel(Application.LOG_INFO);
		font= new BitmapFont(Gdx.files.internal("font.fnt"));
		font1= new BitmapFont(Gdx.files.internal("font1.fnt"));
		skin = new Skin();
		skin.add("font",font);
		UIAtlas = new TextureAtlas("GUI.pack");
		gameAtlas = new TextureAtlas("game.pack");
		skin.addRegions(UIAtlas);
		skin.addRegions(gameAtlas);
		batch = new SpriteBatch();
		titlescene = new TitleScene(this);
		registerscene = new RegisterScene(this);
		loginscene = new LoginScene(this);
		menuscene = new MenuScene(this);
		statusscene = new StatusScene(this);
		matchmakingscene = new MatchmakingScene(this);
		lobbyscene = new LobbyScene(this);
		shopscene= new ShopScene(this);
		accountscene = new AccountScene(this);
		confirmscene = new ConfirmScene(this);
		battlescene = new BattleScene(this);
		scenes.add(titlescene,registerscene,loginscene,menuscene);
		scenes.add(statusscene,matchmakingscene,lobbyscene,shopscene);
		scenes.add(accountscene,confirmscene,battlescene);
		screen= new GameScreen(this);
		setScreen(screen);
	}

	@Override
	public void render () {
		super.render();
	}
	
	@Override
	public void dispose () {
		batch.dispose();
	}

	@Override
	public void resize(int width, int height) {
		WIDTH = width;
		HEIGHT = height;
		screen.updateview();
	}
}
