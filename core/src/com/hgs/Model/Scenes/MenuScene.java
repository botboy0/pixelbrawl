package com.hgs.Model.Scenes;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Align;
import com.hgs.Model.PixelBrawl;
import com.hgs.Model.UI.UIButton;
import com.hgs.Model.UI.UILabel;

public class MenuScene extends Scene {
    UIButton gamebtn;
    UILabel  gamelbl;
    UIButton shopbtn;
    UILabel  shoplbl;
    UIButton accountbtn;
    UILabel  accountlbl;
    public MenuScene(final PixelBrawl pb) {
        super(pb);
        this.type=SceneType.MENU;
        table.setVisible(false);
        table.center().left();
        gamebtn = new UIButton("",table,"controller",new Vector2(150,150),new int[]{0,0,0,0});
        gamebtn.setbackground("controller");
        gamelbl = new UILabel("Play", Align.center,table,"gamelbl",new Vector2(150,50),2f);
        shopbtn = new UIButton("",table,"shop",new Vector2(150,150),new int[]{120,0,0,0});
        shopbtn.setbackground("shop");
        shoplbl = new UILabel("Shop", Align.center,table,"shoplbl",new Vector2(150,50),2f);
        accountbtn = new UIButton("",table,"account",new Vector2(150,150),new int[]{120,0,0,0});
        accountbtn.setbackground("account");
        accountlbl = new UILabel("Account", Align.center,table,"accountlbl",new Vector2(150,50),1.5f);
        table.add(gamebtn.getButton());
        table.row();
        table.add(gamelbl.getlabelcontainer());
        table.row();
        table.add(shopbtn.getButton());
        table.row();
        table.add(shoplbl.getlabelcontainer());
        table.row();
        table.add(accountbtn.getButton());
        table.row();
        table.add(accountlbl.getlabelcontainer());
        components.add(gamebtn,shopbtn,accountbtn);
        components.add(gamelbl,shoplbl,accountlbl);

        gamebtn.getButton().addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                pb.screen.clearscenes();
                pb.screen.showscene(SceneType.MENU);
                pb.screen.showscene(SceneType.MATCHMAKING);
                pb.screen.showscene(SceneType.STATUS);
                if (pb.matchmakingscene.isOpen())
                {
                    pb.matchmakingscene.hidematchlist();
                    pb.screen.showscene(SceneType.LOBBY);
                }
                else
                {
                    pb.matchmakingscene.showmatchlist();
                }
                }
        });

        accountbtn.getButton().addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                pb.screen.clearscenes();
                pb.screen.showscene(SceneType.MENU);
                pb.screen.showscene(SceneType.ACCOUNT);
                pb.accountscene.show();
            }
        });

        shopbtn.getButton().addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                pb.screen.clearscenes();
                pb.screen.showscene(SceneType.MENU);
                pb.screen.showscene(SceneType.SHOP);
            }
        });
    }
}
