package com.hgs.Model.Scenes;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Align;
import com.hgs.Model.PixelBrawl;
import com.hgs.Model.UI.UIButton;
import com.hgs.Model.UI.UILabel;
import com.hgs.Model.UI.UIList;
import com.hgs.Model.UI.UITextfield;

public class MatchmakingScene extends Scene{
    UILabel matchlabel;
    UIList matches;
    UIButton joinbtn;
    UIButton createbtn;
    boolean open;
    public MatchmakingScene(final PixelBrawl pb) {
        super(pb);
        this.type=SceneType.MATCHMAKING;
        table.setVisible(false);
        table.center();
        matchlabel= new UILabel("Matches", Align.center,table,"matchlabel",new Vector2(700,100),1f);
        matches= new UIList(table,"matchlist",new Vector2(700,700),2f);
        joinbtn= new UIButton("Join",table,"joinbtn",new Vector2(300,100),new int[]{50,0,0,0});
        createbtn= new UIButton("Create",table,"createbtn",new Vector2(300,100),new int[]{50,0,0,0});
        table.add(matchlabel.getlabelcontainer()).colspan(2);
        table.row();
        table.add(matches.getContainer()).colspan(2);
        table.row();
        table.add(joinbtn.getButton());
        table.add(createbtn.getButton());
        components.add(matches,matchlabel,joinbtn,createbtn);
        joinbtn.getButton().addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                pb.client.sendCommand("match",new String[]{"join",matches.getList().getSelected()});
            }
        });
        createbtn.getButton().addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                  pb.client.sendCommand("match",new String[]{"create"});
            }
        });


    }

    public UIList getMatches() {
        return matches;
    }


    public void hidematchlist(){
        matches.getContainer().setVisible(false);
        joinbtn.getButton().setVisible(false);
        createbtn.getButton().setVisible(false);
    }
    public void showmatchlist(){
        matches.getContainer().setVisible(true);
        joinbtn.getButton().setVisible(true);
        createbtn.getButton().setVisible(true);
    }

    public boolean isOpen() {
        return open;
    }

    public void setOpen(boolean open) {
        this.open = open;
    }
}
