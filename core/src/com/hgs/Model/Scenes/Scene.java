package com.hgs.Model.Scenes;

import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Array;
import com.hgs.Model.PixelBrawl;
import com.hgs.Model.UI.UIComponent;

public class Scene {
    PixelBrawl pb;
    Table table;
    SceneType type;
    Array<UIComponent>components;
    public Scene(PixelBrawl pb){
        this.pb=pb;
        table= new Table();
        table.setBounds(0,0,PixelBrawl.WIDTH,PixelBrawl.HEIGHT);
        components= new Array<UIComponent>();
    }


    public void  scale(PixelBrawl pb, float scale){
        table.setBounds(0,0,PixelBrawl.WIDTH,PixelBrawl.HEIGHT);
        for (UIComponent uic:components)uic.scale(scale);


    }

    public Table getTable() {
        return table;
    }

    public SceneType getType() {
        return type;
    }

    public Array<UIComponent> getComponents() {
        return components;
    }

}
