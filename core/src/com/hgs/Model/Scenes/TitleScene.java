package com.hgs.Model.Scenes;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Align;
import com.hgs.Model.PixelBrawl;
import com.hgs.Model.UI.UILabel;
import com.hgs.Model.UI.UIButton;

public class TitleScene extends Scene {
    UILabel title;
    UIButton registerbtn;
    UIButton loginbtn;
    public TitleScene(final PixelBrawl pb) {
        super(pb);
        this.type=SceneType.TITLE;
        table.top();
        table.setVisible(true);
        title= new UILabel("PIXEL BRAWL", Align.center,table,"title",new Vector2(600,100),1f,new int[]{50,0,0,0});
        registerbtn= new UIButton("Register",table,"registerbtn",new Vector2(400,200),new int[]{100,0,0,0});
        loginbtn= new UIButton("Login",table,"loginbtn",new Vector2(400,200),new int[]{100,0,0,0});
        table.add(title.getlabelcontainer()).top();
        table.row();
        table.add(registerbtn.getButton());
        table.row();
        table.add(loginbtn.getButton());
        components.add(title,registerbtn,loginbtn);

        registerbtn.getButton().addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                registerbtn.getButton().setVisible(false);
                loginbtn.getButton().setVisible(false);
                pb.screen.showscene(SceneType.REGISTER);
            }
        });

        loginbtn.getButton().addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
            registerbtn.getButton().setVisible(false);
            loginbtn.getButton().setVisible(false);
            pb.screen.showscene(SceneType.LOGIN);
            }
        });
    }

    public UIButton getRegisterbtn() {
        return registerbtn;
    }

    public UIButton getLoginbtn() {
        return loginbtn;
    }
    public void reset(){
        title.getlabelcontainer().setVisible(true);
        loginbtn.getButton().setVisible(true);
        registerbtn.getButton().setVisible(true);
    }
}
