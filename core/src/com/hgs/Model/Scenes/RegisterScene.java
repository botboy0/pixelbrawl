package com.hgs.Model.Scenes;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Align;
import com.hgs.Model.PixelBrawl;
import com.hgs.Model.UI.UILabel;
import com.hgs.Model.UI.UIButton;
import com.hgs.Model.UI.UITextfield;


public class RegisterScene extends Scene {
    UITextfield usernamefield;
    UILabel usernamelabel;
    UITextfield passwordfield;
    UILabel passwordlabel;
    UITextfield passwordrepeatfield;
    UILabel passwordrepeatlabel;
    UIButton registerbutton;
    UIButton backbutton;
    UILabel statuslabel;
    public RegisterScene(final PixelBrawl pb) {
        super(pb);
        this.type= SceneType.REGISTER;
        table.setVisible(false);
        table.top();
        usernamefield = new UITextfield(table,"usernamefield",new Vector2(800,80),false);
        usernamefield.enableFilter();
        usernamelabel = new UILabel("Username:", Align.left,table,"usernamelabel",new Vector2(800,50),0.5f,new int[]{200,0,0,0});
        passwordfield = new UITextfield(table,"passwordfield",new Vector2(800,80),true);
        passwordfield.enableFilter();
        passwordlabel = new UILabel("Password:", Align.left,table,"passwordlabel",new Vector2(800,50),0.5f,new int[]{50,0,0,0});
        passwordrepeatfield = new UITextfield(table,"passwordrepeatfield",new Vector2(800,80),true);
        passwordrepeatfield.enableFilter();
        passwordrepeatlabel = new UILabel("Confirm Password:", Align.left,table,"passwordrepeatlabel",new Vector2(800,50),0.5f,new int[]{50,0,0,0});
        registerbutton = new UIButton("Register",table,"registerbutton",new Vector2(400,200),new int[]{0,0,0,0});
        statuslabel = new UILabel("",Align.center,table,"statuslabel",new Vector2(600, 50),0.5f,new int[]{25,0,25,0});
        backbutton = new UIButton("Back",table,"backbutton",new Vector2(400,200),new int[]{0,0,0,0});
        table.add(usernamelabel.getlabelcontainer()).left().colspan(2);
        table.row();
        table.add(usernamefield.getTextfield()).colspan(2);
        table.row();
        table.add(passwordlabel.getlabelcontainer()).left().colspan(2);
        table.row();
        table.add(passwordfield.getTextfield()).colspan(2);
        table.row();
        table.add(passwordrepeatlabel.getlabelcontainer()).left().colspan(2);
        table.row();
        table.add(passwordrepeatfield.getTextfield()).colspan(2);
        table.row();
        table.add(statuslabel.getlabelcontainer()).colspan(2);
        table.row();
        table.add(backbutton.getButton());
        table.add(registerbutton.getButton());
        components.add(usernamefield,passwordfield,passwordrepeatfield,backbutton);
        components.add(usernamelabel,passwordlabel,passwordrepeatlabel,registerbutton);
        components.add(statuslabel);

        backbutton.getButton().addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                pb.registerscene.getTable().setVisible(false);
                pb.titlescene.getRegisterbtn().getButton().setVisible(true);
                pb.titlescene.getLoginbtn().getButton().setVisible(true);
                statuslabel.getLabel().setText("");
                pb.loginscene.getStatuslabel().getLabel().setText("");
                usernamefield.getTextfield().setText("");
                passwordfield.getTextfield().setText("");
                passwordrepeatfield.getTextfield().setText("");
            }
        });

        registerbutton.getButton().addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                statuslabel.getLabel().setColor(Color.RED);
                if (usernamefield.getTextfield().getText().length()==0){
                    statuslabel.getLabel().setText("Username cant be empty");
                    return;
                }

                if (usernamefield.getTextfield().getText().length()<4){
                    statuslabel.getLabel().setText("Username must be at least 4 characters");
                    return;
                }
                if (passwordfield.getTextfield().getText().length()==0){
                    statuslabel.getLabel().setText("Password cant be empty");
                    return;
                }
                if (passwordfield.getTextfield().getText().length()<6){
                    statuslabel.getLabel().setText("Password must be at least 6 characters");
                    return;
                }
                if (!passwordfield.getTextfield().getText().equals(passwordrepeatfield.getTextfield().getText())){
                    statuslabel.getLabel().setText("Passwords dont match");
                    return;
                }
                statuslabel.getLabel().setColor(Color.WHITE);
                statuslabel.getLabel().setText("Sending...");
                pb.client.sendCommand("register",new String[]{usernamefield.getTextfield().getText(),passwordfield.getTextfield().getText()});
            }
        });

    }

    public UITextfield getUsernamefield() {
        return usernamefield;
    }

    public UITextfield getPasswordfield() {
        return passwordfield;
    }

    public UITextfield getPasswordrepeatfield() {
        return passwordrepeatfield;
    }

    public UILabel getStatuslabel() {
        return statuslabel;
    }

    public void reset(){
        usernamefield.getTextfield().setText("");
        passwordfield.getTextfield().setText("");
        passwordrepeatfield.getTextfield().setText("");
        statuslabel.getLabel().setText("");
    }
}
