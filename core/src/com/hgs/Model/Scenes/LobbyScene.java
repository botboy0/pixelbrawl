package com.hgs.Model.Scenes;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Align;
import com.hgs.Model.PixelBrawl;
import com.hgs.Model.UI.UIButton;
import com.hgs.Model.UI.UILabel;

public class LobbyScene extends Scene {
    UILabel waitinglabel;
    UIButton cancelbtn;
    public LobbyScene(final PixelBrawl pb) {
        super(pb);
        this.type=SceneType.LOBBY;
        table.setVisible(false);
        table.center();
        waitinglabel = new UILabel("Waiting For Opponent \n ", Align.center,table,"waitinglabel",new Vector2(600,200),1f,new int[]{150,0,100,0});
        cancelbtn =  new UIButton("Cancel",table,"cancelbtn",new Vector2(600,200),new int[]{0,0,0,0});
        table.add(waitinglabel.getlabelcontainer());
        table.row();
        table.add(cancelbtn.getButton());
        components.add(waitinglabel,cancelbtn);

        cancelbtn.getButton().addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                pb.client.sendCommand("match",new String[]{"close"});
            }
        });
    }
    public void showlobby(){
        table.setVisible(true);
    };
    public void hidelobby(){
        table.setVisible(false);
    }
}
