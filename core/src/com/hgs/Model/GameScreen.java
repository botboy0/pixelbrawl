package com.hgs.Model;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.hgs.Model.Scenes.Scene;
import com.hgs.Model.Scenes.SceneType;

public class GameScreen implements Screen {
    PixelBrawl pb;
    Stage stage;
    float widthratio;
    float heightratio;

    public  GameScreen(PixelBrawl pb){
     this.pb=pb;
     ScreenViewport screenViewport = new ScreenViewport();
     stage = new Stage(screenViewport);
        for (Scene scene:pb.scenes)stage.addActor(scene.getTable());
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void show() {
        widthratio = (float) PixelBrawl.WIDTH/ PixelBrawl.oWIDTH;
        heightratio = (float) PixelBrawl.HEIGHT/ PixelBrawl.oHEIGHT;

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0/255f, 0/255f, 0/255f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        pb.battlescene.stepanimations(pb.battlescene.getWeaponanim(),pb.battlescene.getSpellanim(),delta);
        stage.act();
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {
        if (pb.client.isIngame()) pb.client.sendCommand("match",new String[]{"timerrequest"});
    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
        pb.batch.dispose();
    }

    public void updateview() {
        stage.getViewport().update(PixelBrawl.WIDTH, PixelBrawl.HEIGHT);
        stage.getCamera().position.set(PixelBrawl.WIDTH/2, PixelBrawl.HEIGHT/2,0);
        stage.getCamera().update();
        widthratio = (float) PixelBrawl.WIDTH/ PixelBrawl.oWIDTH;
        heightratio = (float) PixelBrawl.HEIGHT/ PixelBrawl.oHEIGHT;
        for (Scene scene:pb.scenes)scene.scale(pb,widthratio<heightratio?widthratio:heightratio);
        if (pb.battlescene.getTarget()!=null){
            if (pb.battlescene.getTarget()==pb.battlescene.getOpponent())
            {
                pb.battlescene.scaleanimations(pb.battlescene.getWeaponanim(),pb.battlescene.getSpellanim(),pb.battlescene.getplayerpos(), pb.battlescene.getopponentpos(), widthratio < heightratio ? widthratio : heightratio);
            }
            else
            {
                pb.battlescene.scaleanimations(pb.battlescene.getWeaponanim(),pb.battlescene.getSpellanim(),pb.battlescene.getopponentpos(), pb.battlescene.getplayerpos(), widthratio < heightratio ? widthratio : heightratio);
            }
        }
    }

    public void clearscenes(){
        for (Scene scene:pb.scenes)scene.getTable().setVisible(false);
    }
    public void showscene(SceneType type){
        for (Scene scene:pb.scenes)if (scene.getType()==type)scene.getTable().setVisible(true);
    }
    public void hidescene(SceneType type){
        for (Scene scene:pb.scenes)if (scene.getType()==type)scene.getTable().setVisible(false);
    }

    public Stage getStage() {
        return stage;
    }
}
